﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TechnicalAssessment.Startup))]
namespace TechnicalAssessment
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
