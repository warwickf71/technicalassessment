﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechnicalAssessment.Models
{
    public class Triangulator
    {
        public Triangulator(int SideA, int SideB, int SideC)
        {
            this.SideA = SideA;
            this.SideB = SideB;
            this.SideC = SideC;
        }

        public Triangulator()
        {
            this.SideA = 1;
            this.SideB = 1;
            this.SideC = 1;  
        }

        public double SideA { get; set; }
        public double SideB { get; set; }
        public double SideC { get; set; }

        public bool IsRight
        {
            get
            {
                if (SideA * SideA == SideB * SideB + SideC * SideC)
                    return true;
                if (SideB * SideB == SideA * SideA + SideC * SideC)
                    return true;
                if (SideC * SideC == SideB * SideB + SideA * SideA)
                    return true;

                return false;
            }
        }
        public bool IsEquilateral
        {
            get
            {
                if (SideA == SideB && SideB == SideC)
                    return true;

                return false;
            }
        }
        public bool IsIsoscles
        {
            get
            {
               if (SideA == SideB && SideA != SideC)
                    return true;
                if (SideC == SideB && SideA != SideC)
                    return true;
                if (SideA == SideC && SideA != SideB)
                    return true;

                return false;
            }
        }
        public bool IsScalene
        {
            get
            {
                if (SideA == SideB / 2 && SideA == SideC / 3)
                    return true;
                if (SideA == SideC / 2 && SideA == SideB / 3)
                    return true;
                if (SideB == SideA / 2 && SideB == SideC / 3)
                    return true;
                if (SideB == SideC / 2 && SideB == SideA / 3)
                    return true;
                if (SideC == SideA / 2 && SideC == SideB / 3)
                    return true;
                if (SideC == SideB / 2 && SideC == SideB / 3)
                    return true;

                return false;
            }
        }
        public bool IsObtuse
        {
            get
            {
                double cosA = Math.Acos((SideB * SideB + SideC * SideC - SideA * SideA) / (2 * SideB * SideC)) * (180.0 / Math.PI);
                double cosB = Math.Acos((SideA * SideA + SideC * SideC - SideB * SideB) / (2 * SideA * SideC)) * (180.0 / Math.PI);
                double cosC = Math.Acos((SideB * SideB + SideA * SideA - SideC * SideC) / (2 * SideB * SideA)) * (180.0 / Math.PI);

                if (cosA > 90 || cosB > 90 || cosC > 90)
                    return true;

                return false;
            }
        }
        public bool IsAcute
        {
            get
            {
                double cosA = Math.Acos((SideB * SideB + SideC * SideC - SideA * SideA) / (2 * SideB * SideC)) * (180.0 / Math.PI);
                double cosB = Math.Acos((SideA * SideA + SideC * SideC - SideB * SideB) / (2 * SideA * SideC)) * (180.0 / Math.PI);
                double cosC = Math.Acos((SideB * SideB + SideA * SideA - SideC * SideC) / (2 * SideB * SideA)) * (180.0 / Math.PI);

                if (cosA < 90 && cosB < 90 && cosC < 90)
                    return true;

                return false;
            }
        }
        public bool IsInvalid
        {
            get
            {
                if (SideA > SideA + SideB)
                    return true;
                if (SideB > SideA + SideC)
                    return true;
                if (SideC > SideA + SideB)
                    return true;

                return false;
            }
        }
    }
}