﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechnicalAssessment.Models
{
    public class ListTraverser
    {
        public LinkedList<int> ListOfIntegers { get; set; }

        public ListTraverser()
        {
                
        }

        public ListTraverser(LinkedList<int> list)
        {
            ListOfIntegers = list;
        }

        public int GetFifthItemFromEnd()
        {
            return ListOfIntegers.Last.Previous.Previous.Previous.Previous.Value; 
        }
    }
}