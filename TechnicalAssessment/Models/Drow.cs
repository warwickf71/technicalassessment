﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Profile;

namespace TechnicalAssessment.Models
{
    public class Drow
    {
        public Drow(string Value)
        {
            this.Value = Value;
        }

        public Drow()
        {
                
        }

        public string Value { get; set; }
        
        public string Reversed
        {
            get
            {
                string result = "";
                string shelf = "";
                foreach (char character in Value)
                {
                    Regex regex = new Regex(@"[a-zA-Z]");
                    Match match = regex.Match(character.ToString());
	                if (match.Success)
                        shelf = character + shelf;
                    else
                    {
                        result += shelf + character;
                        shelf = "";
                    }
                }
                if (string.IsNullOrEmpty(shelf))
                    result += shelf;
                return result;
            }
        }
    }
}