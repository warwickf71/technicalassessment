﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TechnicalAssessment.Models;

namespace TechnicalAssessment.Controllers
{
    public class ListTraverserController : Controller
    {
        // GET: ListTraverser
        public ActionResult Index()
        {
            LinkedList<int> list = new LinkedList<int>();
            list.AddLast(1);
            list.AddLast(2);
            list.AddLast(3);
            list.AddLast(4);
            list.AddLast(5);
            list.AddLast(6);
            list.AddLast(7);
            return View(new ListTraverser(list));
        }
        // GET: ListTraverser
        public ActionResult Index2(ListTraverser listTraverser)
        {
            return View(listTraverser);
        }
    }
}