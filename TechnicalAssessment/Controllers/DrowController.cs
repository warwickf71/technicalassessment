﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TechnicalAssessment.Models;

namespace TechnicalAssessment.Controllers
{
    public class DrowController : Controller
    {
        // GET: Drow
        public ActionResult Index()
        {
            return View(new Drow("This is a Sample. 123. Of backwards Text."));
        }

        [HttpPost]
        public ActionResult Index(Drow Drow)
        {
            return View(Drow);
        }
    }
}