﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TechnicalAssessment.Models;

namespace TechnicalAssessment.Controllers
{
    public class TriangulatorController : Controller
    {
        // GET: Triangulator
        public ActionResult Index()
        {
            return View(new Triangulator(3, 4, 5));
        }

        [HttpPost]
        public ActionResult Index(Triangulator triangulator)
        {
            return View(triangulator);
        }
    }
}